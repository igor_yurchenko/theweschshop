package com.lgi.theweschshop.shopdata.controller;

import com.lgi.theweschshop.shopdata.model.Element;
import com.lgi.theweschshop.shopdata.model.Type;
import com.lgi.theweschshop.shopdata.requests.ElementSaveRequestDTO;
import com.lgi.theweschshop.shopdata.service.ElementService;
import com.lgi.theweschshop.shopdata.service.TypeService;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igor Yurchenko on 10/26/17.
 */
@Controller
public class ElementController extends WebMvcConfigurerAdapter{

    private static final Logger l = LoggerFactory.getLogger( "" );

    @Autowired
    private ElementService elementService;

    @Autowired
    private TypeService typeService;


    @Override
    public void addViewControllers( ViewControllerRegistry registry ) {
        registry.addViewController( "/" ).setViewName( "index" );
        registry.addViewController( "/index" ).setViewName( "index" );
        registry.addViewController( "/hello" ).setViewName( "hello" );
        registry.addViewController( "/shop" ).setViewName( "shop" );
//        registry.addViewController( "/add" ).setViewName( "add" );
//        registry.addViewController( "/login" ).setViewName( "login" );

    }

    @GetMapping("/")
    public String index() {

        return "index";
    }

    @GetMapping("shop")
    public String list( @RequestParam @NotEmpty String type, Pageable pageable, Model model ) {
        Type typeByTypeName = typeService.findTypeByTypeName( type );
        if ( typeByTypeName == null ) {
            typeByTypeName = typeService.getDefaultType();
        }
        List<Element> allElements = elementService.getElementsListByType( typeByTypeName, pageable );
        if ( allElements == null ) {
            allElements = new ArrayList<>();
        }
        int size = allElements.size();
        if ( size < 1 ) {
            size = 1;
        }
        model.addAttribute( "rows", size );
        model.addAttribute( "elements", allElements );
        return "shop";
    }

    @PostMapping("add")
    public String addElement( @RequestParam @Valid ElementSaveRequestDTO element ) {

//        elementService.addElement( element );

        return "redirect:/shop";
    }
//
//    @PostMapping("delete")
//    public String delete( Model model, @RequestParam Number elementId ) {
//        Optional<Element> elementById = elementService.getElementById( elementId );
//        Element element = elementById.orElseThrow( NoSuchElementException::new );
//        elementService.delete( element );
//        return "redirect:/element/list";
//    }
}
